#ifndef CUSTOMTREEVIEW_H
#define CUSTOMTREEVIEW_H

#include "view_common_interface.h"
#include "config_reference.hpp"
#include "dbcontroller.h"

#include <config/ConfigObject.h>

#include <ers/ers.h>

#include <QTreeView>
#include<QUuid>


namespace dbe
{
class CustomTreeView: public QTreeView , public view_common_interface
{
  Q_OBJECT
public:
  CustomTreeView ( QWidget * Parent = nullptr );
  void contextMenuEvent ( QContextMenuEvent * Event );

protected:
  void closeEvent(QCloseEvent * event) override;

private slots:
  void slot_delete_objects();
  void slot_create_object();
  void slot_edit_object();
  void slot_copy_object();

  void referencedbyOnlycomposite();
  void referencedByAll();

public slots:
  void referencedBy ( bool All );
  void referencedBy ( bool All, tref Object );
signals:
  void OpenEditor ( tref Object );

private:
  void edit_object ( QModelIndex const & );
  void CreateActions();

  QMenu * contextMenu;
  QAction * editObjectAc;
  QAction * deleteObjectAc;
  QAction * createObjectAc;
  QAction * copyObjectAc;
  QAction * deleteObjectWidgetAc;
  QAction * hideShowAc;
  QAction * buildTableFromClassAc;
  QAction * expandAllAc;
  QAction * colapseAllAc;
  QAction * refByAc;
  QAction * refByAcOnlyComp;
};
} // end namespace dbe
#endif // CUSTOMTREEVIEW_H
