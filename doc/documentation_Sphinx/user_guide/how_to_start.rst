
How to start DBE
================

Setup the release, for example: 

.. code-block:: bash

  source /afs/cern.ch/atlas/project/tdaq/cmt/bin/cmtsetup.sh tdaq-04-00-01


Start the editor at command line:  

.. code-block:: bash

  dbe

You can also specify a DB file at command line, for example:

.. code-block:: bash

  dbe path-to-file/file.data.xml


To see all the command-line options (*more to be added*) type:

.. code-block:: bash

  dbe --help

