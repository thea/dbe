
========================
Developer's Notes
========================


.. include:: ../small_beginning_note.rst 

----

.. highlight:: c++
   :linenothreshold: 5

Developer's Guide:
==================

.. toctree::
   :maxdepth: 2

   compilation
   documentation

----

**The DBE JIRA project and User's support:**

https://its.cern.ch/jira/browse/ATLASDBE

----

**Doxygen documentation:**

https://atlasdaq.cern.ch/dbe/doxygen_doc/html/

----

Some tech details:
==================

.. toctree::
   :maxdepth: 2

   db_communication
   create_new_obj
   resources
   model_view
   positioning



----

.. toctree::

   validation_tests

----


.. toctree::
   :maxdepth: 1

   changes


----

(Only for reference: `Old User Support site - Issue Tracking <https://espace.cern.ch/atlas-tdaq-cc/Lists/DBE%20users%20feedback/AllItems.aspx>`_)

